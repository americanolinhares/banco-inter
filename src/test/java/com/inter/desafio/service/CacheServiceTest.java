package com.inter.desafio.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CacheServiceTest {

    CacheService cacheService;

    @Before
    public void init() {
        cacheService = new CacheService();
    }

    @Test
    public void construtor_deveCriarCacheCorretamente() {

        assertNotNull(cacheService);
        assertEquals(0, cacheService.tamanho());
    }

    @Test
    public void put_deveInserirObjetoNoCacheCorretamente() {

        cacheService.put("Digito [numero=1010, repeticoesNumero=4]", 1);

        assertNotNull(cacheService);
        assertEquals(1, cacheService.tamanho());
    }

    @Test
    public void getObjetoExistente_deveRetornarObjetoDoCacheCorretamente() {

        cacheService.put("Digito [numero=1010, repeticoesNumero=4]", 1);
        int valorRetornado = cacheService.get("Digito [numero=1010, repeticoesNumero=4]");
        assertEquals(1, valorRetornado);
    }

    @Test
    public void getObjetoNaoExistente_deveRetornarNulo() {

        assertNull(cacheService.get("Digito [numero=0010, repeticoesNumero=4]"));
    }

    @Test
    public void tamanho_deveRetornarTamhoDoCacheCorretamente() {

        cacheService.put("Digito [numero=1010, repeticoesNumero=4]", 8);
        cacheService.put("Digito [numero=5234, repeticoesNumero=4]", 2);
        cacheService.put("Digito [numero=0010, repeticoesNumero=4]", 4);

        assertNotNull(cacheService);
        assertEquals(3, cacheService.tamanho());
    }

    @Test
    public void contem_deveRetornarVerdadeiro() {

        cacheService.put("Digito [numero=1010, repeticoesNumero=4]", 8);
        assertEquals(true, cacheService.contem("Digito [numero=1010, repeticoesNumero=4]"));
    }

}
