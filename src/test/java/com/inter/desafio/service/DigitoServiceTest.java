package com.inter.desafio.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.repository.DigitoRepository;
import com.inter.desafio.repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class DigitoServiceTest {

    @InjectMocks
    private DigitoService digitoService;

    @Mock
    DigitoRepository digitoRepository;

    @Mock
    UsuarioRepository usuarioRepository;

    @Mock
    CacheService cacheService;

    Digito digito1, digito2;
    Usuario usuario1;

    @Before
    public void init() {

        digito1 = new Digito(1, "111", 4);
        digito2 = new Digito(2, "101", 5, 1);

        usuario1 = new Usuario(1, "Maria", "maria@bancointer.com");
    }


    @Test
    public void obterTodosDigitos_deveRetornarTodosDigitosCorretamente() {

        when(digitoRepository.findAll()).thenReturn(Arrays.asList(digito1, digito2));

        List<Digito> todosDigitos = digitoService.obterTodosDigitos();

        assertEquals("111", todosDigitos.get(0).getNumero());
        assertEquals(1, todosDigitos.get(1).getDigitoUnico());
        assertEquals(2, todosDigitos.size());
    }

    @Test
    public void salvarDigitoSemUsuarioAssociado_deveSalvarDigitoCorretamente() {

        when(digitoRepository.save(isA(Digito.class))).thenAnswer(returnsFirstArg());

        digitoService.salvarDigito(digito1, "0");
        ArgumentCaptor<Digito> argumentoDigitoSalvo = ArgumentCaptor.forClass(Digito.class);

        verify(digitoRepository, times(1)).save(argumentoDigitoSalvo.capture());
        verifyNoMoreInteractions(digitoRepository);

        assertEquals(4, argumentoDigitoSalvo.getValue().getRepeticoesNumero());
        assertEquals(1, argumentoDigitoSalvo.getValue().getId());
    }

    @Test
    public void salvarDigitoComUsuarioAssociadoValido_deveSalvarDigitoCorretamente() {


        usuario1.setListaDigitosUnicos(Arrays.asList(digito1));

        when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario1));
        when(usuarioRepository.save(isA(Usuario.class))).thenAnswer(returnsFirstArg());

        digitoService.salvarDigito(digito1, "1");
        ArgumentCaptor<Usuario> usuarioSalvo = ArgumentCaptor.forClass(Usuario.class);

        verify(usuarioRepository, times(1)).save(usuarioSalvo.capture());
        verifyNoMoreInteractions(digitoRepository);

        assertEquals(2, usuarioSalvo.getValue().getListaDigitosUnicos().size());
        assertEquals("maria@bancointer.com", usuarioSalvo.getValue().getEmail());
        assertEquals("111", usuarioSalvo.getValue().getListaDigitosUnicos().get(0).getNumero());
    }

    @Test
    public void salvarDigitoComUsuarioAssociadoInvalido_deveRetornarZero() {

        when(usuarioRepository.findById(5)).thenReturn(Optional.empty());
        assertEquals(0, digitoService.salvarDigito(digito1, "5").getDigitoUnico());
    }

    @Test
    public void adicionarDigitoAUsuario_deveAdicionarCalculoNaListaDoUsuario() {

        usuario1.setListaDigitosUnicos(Arrays.asList(digito1));
        digitoService.adicionarDigitoAUsuario(digito2, usuario1);
        assertEquals(2, usuario1.getListaDigitosUnicos().size());
    }

    @Test
    public void inserirCalculoNoCache_deveInserirCalculoNoCacheCorretamente() {

        digitoService.inserirCalculoNoCache(digito1);
        assertNotNull(digitoService);
    }

    @Test
    public void estaNoCacheCalculoExistente_deveRetornarVerdadeiro() {

        when(cacheService.contem(digito1.toString())).thenReturn(true);
        assertEquals(true, digitoService.digitoEstaNoCache(digito1));
    }

    @Test
    public void estaNoCacheCalculoInexistente_deveRetornarFalso() {

        when(cacheService.get(digito1.toString())).thenReturn(null);
        assertEquals(false, digitoService.digitoEstaNoCache(digito1));
    }

    @Test
    public void obterCalculoDoCacheQuandoCalculoDiferenteZeroMultiploNove_deveRetornarCalculoDoCacheCorretamente() {

        digito1 = digitoService.calcularDigitoUnico(digito1);
        when(cacheService.get(digito1.toString())).thenReturn(digito1.getDigitoUnico());
        assertEquals(3, digitoService.obterCalculoDoCache(digito1));
    }

    @Test
    public void obterCalculoDoCacheQuandoCalculoMultiploNove_deveRetornarCalculoDoCacheCorretamente() {

        Digito digito3 = new Digito(3, "999", 5);
        digito3 = digitoService.calcularDigitoUnico(digito3);

        when(cacheService.get(digito3.toString())).thenReturn(digito3.getDigitoUnico());
        assertEquals(9, digitoService.obterCalculoDoCache(digito3));
    }

    @Test
    public void obterCalculoDoCacheQuandoCalculoZero_deveRetornarCalculoDoCacheCorretamente() {

        Digito digito4 = new Digito(4, "0", 1);
        digito4 = digitoService.calcularDigitoUnico(digito4);

        when(cacheService.get(digito4.toString())).thenReturn(digito4.getDigitoUnico());
        assertEquals(0, digitoService.obterCalculoDoCache(digito4));
    }

    @Test
    public void calcularDigitoUnico_deveRetornarObjetoDigitoComCampoDigitoUnicoCalculado() {

        digito1 = digitoService.calcularDigitoUnico(digito1);
        assertEquals(3, digito1.getDigitoUnico());
    }
}
