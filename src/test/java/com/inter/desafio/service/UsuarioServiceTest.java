package com.inter.desafio.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class UsuarioServiceTest {

    @InjectMocks
    private UsuarioService usuarioService;

    @Mock
    UsuarioRepository usuarioRepository;

    Usuario usuario1;

    Digito digito1, digito2;

    @Before
    public void init() {

        digito1 = new Digito(1, "56", 4, 8);
        digito2 = new Digito(2, "10", 5, 5);

        usuario1 = new Usuario(1, "Maria", "maria@bancointer.com", Arrays.asList(digito1, digito2));
    }


    @Test
    public void obterTodosUsuarios_deveRetornarTodosUsuariosCorretamente() {

        Usuario usuario2 = new Usuario(2, "Joao", "joao@bancointer.com");

        when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario1, usuario2));

        List<Usuario> todosUsuarios = usuarioService.obterTodosUsuarios();

        assertEquals("Maria", todosUsuarios.get(0).getNome());
        assertEquals(8, todosUsuarios.get(0).getListaDigitosUnicos().get(0).getDigitoUnico());
        assertEquals("joao@bancointer.com", todosUsuarios.get(1).getEmail());
        assertEquals(2, todosUsuarios.size());
    }

    @Test
    public void obterUsuarioPorIdCorreto_deveRetornarUsuarioCorretamente() {

        when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario1));

        Usuario usuario = usuarioService.obterUsuarioPorId(1);

        assertEquals("Maria", usuario.getNome());
        assertEquals("maria@bancointer.com", usuario.getEmail());
        assertEquals("56", usuario.getListaDigitosUnicos().get(0).getNumero());
    }

    @Test(expected = NoSuchElementException.class)
    public void obterUsuarioPorIdIncorreto_deveGerarExcecao() throws NoSuchElementException {

        when(usuarioRepository.findById(1)).thenReturn(Optional.empty());

        usuarioService.obterUsuarioPorId(1);
    }

    @Test
    public void deletar_deveDeletarUsuarioCorretamente() {

        when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario1));

        usuarioService.deletarUsuarioPorId(1);

        verify(usuarioRepository, times(1)).deleteById(1);
    }

    @Test
    public void salvar_deveSalvarUsuarioCorretamente() {

        when(usuarioRepository.save(isA(Usuario.class))).thenAnswer(returnsFirstArg());

        usuarioService.salvarOuAtualizarUsuario(usuario1);
        ArgumentCaptor<Usuario> argumentoUsuarioSalvo = ArgumentCaptor.forClass(Usuario.class);

        verify(usuarioRepository, times(1)).save(argumentoUsuarioSalvo.capture());
        verifyNoMoreInteractions(usuarioRepository);

        assertEquals("Maria", argumentoUsuarioSalvo.getValue().getNome());
        assertEquals("maria@bancointer.com", argumentoUsuarioSalvo.getValue().getEmail());
    }

    @Test
    public void obterCalculoUsuarioPorIdCorreto_deveRetornarCalculosCorretamente() {

        when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario1));

        List<Digito> listaDigitosUnicos = usuarioService.obterCalculoUsuarioPorId(1);

        assertEquals(2, listaDigitosUnicos.size());
        assertEquals(8, listaDigitosUnicos.get(0).getDigitoUnico());
    }

    @Test
    public void existeUsuarioPorIdCorreto_deveRetornarVerdadeiro() {

        when(usuarioRepository.existsById(1)).thenReturn(true);
        assertEquals(true, usuarioService.existeUsuarioPorId(1));
    }
}
