package com.inter.desafio.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class UsuarioTest {

    private Usuario usuario1;
    List<Digito> listaDigitos;
    Digito digito1, digito2;

    @Before
    public void init() {

        digito1 = new Digito("5", 4);
        digito2 = new Digito("6", 3);
        listaDigitos = Arrays.asList(digito1, digito2);

        usuario1 = new Usuario(1, "Frederico", "dev@bancointer.com", listaDigitos);
    }

    @Test
    public void getId_deveRetornarIdCorretamente() {
        assertEquals(1, usuario1.getId());
    }

    @Test
    public void getNome_deveRetornarNomeCorretamente() {
        assertEquals("Frederico", usuario1.getNome());
    }

    @Test
    public void getEmail_deveRetornarEmailCorretamente() {
        assertEquals("dev@bancointer.com", usuario1.getEmail());
    }

    @Test
    public void getListaDigitosUnicos_deveRetornarListaDigitosunicosCorretamente() {
        assertEquals(2, usuario1.getListaDigitosUnicos().size());
        assertEquals(digito1, usuario1.getListaDigitosUnicos().get(0));
    }

    @Test
    public void contrutorSemListaDigitos_deveCriarUsuarioCorretamente() {

        Usuario usuario2 = new Usuario(2, "iza", "iza@bancointer.com");

        assertEquals(2, usuario2.getId());
        assertEquals("iza@bancointer.com", usuario2.getEmail());
        assertEquals(Collections.EMPTY_LIST, usuario2.getListaDigitosUnicos());
    }

    @Test
    public void contrutorPadrao_deveCriarUsuarioCorretamente() {

        Usuario usuario3 = new Usuario();
        assertNotNull(usuario3);
    }


}
