package com.inter.desafio.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class DigitoTest {

    Digito digito1;

    @Before
    public void init() {

        digito1 = new Digito(1, "56", 4, 8);
    }

    @Test
    public void getId_deveRetornarIdCorretamente() {
        assertEquals(1, digito1.getId());
    }

    @Test
    public void getNome_deveRetornarNumeroCorretamente() {
        assertEquals("56", digito1.getNumero());
    }

    @Test
    public void getEmail_deveRetornarRepeticoesNumeroCorretamente() {
        assertEquals(4, digito1.getRepeticoesNumero());
    }

    @Test
    public void getEmail_deveRetornarDigitoUnicoCorretamente() {
        assertEquals(8, digito1.getDigitoUnico());
    }

    @Test
    public void toString_deveRetornarStringCorretamente() {
        assertEquals("Digito [numero=56, repeticoesNumero=4]", digito1.toString());
    }

    @Test
    public void construtorComNumeroERepeticoesNumero_deveCriarDigitoCorretamente() {

        Digito digito2 = new Digito("45698", 3);

        assertNotNull(digito2);
        assertEquals("Digito [numero=45698, repeticoesNumero=3]", digito2.toString());
    }

    @Test
    public void construtorComIdNumeroERepeticoesNumero_deveCriarDigitoCorretamente() {

        Digito digito3 = new Digito(3, "45698", 3);

        assertNotNull(digito3);
        assertEquals(3, digito3.getId());
        assertEquals("Digito [numero=45698, repeticoesNumero=3]", digito3.toString());
    }


    @Test
    public void contrutorPadrao_deveCriarDigitoCorretamente() {

        Digito digito4 = new Digito();
        assertNotNull(digito4);
    }

    @Test
    public void set_deveIniciarCamposCorretamente() {

        Digito digito5 = new Digito();
        digito5.setId(5);
        digito5.setNumero("10011");
        digito5.setRepeticoesNumero(4);
        digito5.setDigitoUnico(3);


        assertEquals(5, digito5.getId());
        assertEquals(3, digito5.getDigitoUnico());
        assertEquals("Digito [numero=10011, repeticoesNumero=4]", digito5.toString());

    }
}
