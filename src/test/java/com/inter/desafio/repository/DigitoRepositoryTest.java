package com.inter.desafio.repository;

import static org.junit.Assert.assertTrue;
import com.inter.desafio.model.Digito;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DigitoRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    DigitoRepository digitoRepository;

    @Test
    public void salvar_deveSalvarUsuario() {

        Digito digito = new Digito();
        digito.setNumero("54");
        digito.setRepeticoesNumero(4);
        digito.setDigitoUnico(9);

        digito = entityManager.persistAndFlush(digito);

        assertTrue(digitoRepository.findById(digito.getId()).get().equals(digito));
    }

}
