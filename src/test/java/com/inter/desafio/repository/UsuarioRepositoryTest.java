package com.inter.desafio.repository;

import static org.junit.Assert.assertTrue;
import com.inter.desafio.model.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryTest {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Test
    public void salvar_deveSalvarUsuario() {

        Usuario usuario = new Usuario();
        usuario.setNome("Norma");
        usuario.setEmail("norma@inter.com");

        usuario = entityManager.persistAndFlush(usuario);

        assertTrue(usuarioRepository.findById(usuario.getId()).get().equals(usuario));
    }

}
