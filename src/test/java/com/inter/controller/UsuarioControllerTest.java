package com.inter.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import java.util.List;
import com.inter.desafio.controller.UsuarioController;
import com.inter.desafio.filter.CorsFilter;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.service.UsuarioService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class UsuarioControllerTest {

    @InjectMocks
    private UsuarioController usuarioController;

    @Mock
    private UsuarioService usuarioService;

    private MockMvc mockMvc;

    Usuario usuario1, usuario2;

    Digito digito1, digito2;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(usuarioController).addFilters(new CorsFilter()).build();

        digito1 = new Digito(1, "56", 4, 8);
        digito2 = new Digito(2, "10", 5, 5);

        usuario1 = new Usuario(1, "Maria", "maria@bancointer.com", Arrays.asList(digito1, digito2));
        usuario2 = new Usuario(2, "Joao", "joao@bancointer.com");
    }

    @Test
    public void obterTodosUsuarios_deveRetornarComSucesso() throws Exception {

        List<Usuario> usuarios = Arrays.asList(usuario1, usuario2);
        when(usuarioService.obterTodosUsuarios()).thenReturn(usuarios);

        mockMvc.perform(get("/usuarios")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nome", is("Maria")))
                .andExpect(jsonPath("$[0].listaDigitosUnicos.[0].id", is(1)))
                .andExpect(jsonPath("$[0].listaDigitosUnicos.[1].numero", is("10")))
                .andExpect(jsonPath("$[1].id", is(2))).andExpect(jsonPath("$[1].email", is("joao@bancointer.com")));

        verify(usuarioService, times(1)).obterTodosUsuarios();
        verifyNoMoreInteractions(usuarioService);
    }

    @Test
    public void obterUsuarioPorIdCorreto_deveRetornarComSucesso() throws Exception {

        when(usuarioService.obterUsuarioPorId(1)).thenReturn(usuario1);
        when(usuarioService.existeUsuarioPorId(1)).thenReturn(true);

        mockMvc.perform(get("/usuarios/{id}", 1)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.nome", is("Maria")))
                .andExpect(jsonPath("$.listaDigitosUnicos.[0].id", is(1)));

        verify(usuarioService, times(1)).obterUsuarioPorId(1);
        verify(usuarioService, times(1)).existeUsuarioPorId(1);
        verifyNoMoreInteractions(usuarioService);
    }

    @Test
    public void obterUsuarioPorIdIncorreto_deveRetornarNaoEncontrado() throws Exception {

        when(usuarioService.existeUsuarioPorId(3)).thenReturn(false);

        mockMvc.perform(get("/usuarios/{id}", 3)).andExpect(status().isNotFound());

        verify(usuarioService, times(1)).existeUsuarioPorId(3);
        verifyNoMoreInteractions(usuarioService);
    }

    @Test
    public void criarUsuario_deveRetornarCriadoComSucesso() throws Exception {

        Usuario usuario3 = new Usuario();
        usuario3.setNome("Fred");
        usuario3.setEmail("fred@bancointer.com");

        String usuarioJson = "{\"nome\":\"Fred\",\"email\":\"fred@bancointer.com\"}";

        when(usuarioService.salvarOuAtualizarUsuario(Mockito.any(Usuario.class))).thenReturn(usuario3);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/usuarios").accept(MediaType.APPLICATION_JSON)
                .content(usuarioJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.nome", is("Fred"))).andExpect(jsonPath("$.email", is("fred@bancointer.com")));

        verify(usuarioService, times(1)).salvarOuAtualizarUsuario(Mockito.any(Usuario.class));
    }

    @Test
    public void atualizarUsuario_deveRetornarSucesso() throws Exception {

        Usuario usuario1Atualizado = new Usuario(1, "Maria Clara", "mariaclara@bancointer.com");

        String usuarioAtualizadoJson =
                "{\"id\":\"1\",\"nome\":\"Maria Clara\",\"email\":\"mariaclara@bancointer.com\"}";

        when(usuarioService.salvarOuAtualizarUsuario(Mockito.any(Usuario.class))).thenReturn(usuario1Atualizado);

        RequestBuilder requestBuilderAtualizacao =
                MockMvcRequestBuilders.post("/usuarios").accept(MediaType.APPLICATION_JSON)
                        .content(usuarioAtualizadoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilderAtualizacao).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.nome", is("Maria Clara")))
                .andExpect(jsonPath("$.email", is("mariaclara@bancointer.com")));

        verify(usuarioService, times(1)).salvarOuAtualizarUsuario(Mockito.any(Usuario.class));
    }

    @Test
    public void deletarUsuarioExistente_deveRetornarSucesso() throws Exception {

        when(usuarioService.existeUsuarioPorId(usuario1.getId())).thenReturn(true);
        doNothing().when(usuarioService).deletarUsuarioPorId(usuario1.getId());

        mockMvc.perform(delete("/usuarios/{id}", usuario1.getId())).andExpect(status().isOk());

        verify(usuarioService, times(1)).existeUsuarioPorId(usuario1.getId());
        verify(usuarioService, times(1)).deletarUsuarioPorId(usuario1.getId());
        verifyNoMoreInteractions(usuarioService);
    }

    @Test
    public void deletarUsuarioInexistente_deveRetornarNaoEncontrado() throws Exception {

        when(usuarioService.obterUsuarioPorId(1)).thenReturn(null);
        doNothing().when(usuarioService).deletarUsuarioPorId(usuario1.getId());

        mockMvc.perform(delete("/usuarios/{id}", usuario1.getId())).andExpect(status().isNotFound());
    }

    @Test
    public void obterCalculoUsuario_deveRetornarSucesso() throws Exception {

        when(usuarioService.obterCalculoUsuarioPorId(usuario1.getId())).thenReturn(Arrays.asList(digito1, digito2));

        mockMvc.perform(get("/usuarios/calculos/{id}", usuario1.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].numero", is("56"))).andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].repeticoesNumero", is(5)));

        verify(usuarioService, times(1)).obterCalculoUsuarioPorId(usuario1.getId());
        verifyNoMoreInteractions(usuarioService);
    }

    @Test
    public void cors_headers() throws Exception {
        mockMvc.perform(get("/usuarios")).andExpect(header().string("Access-Control-Allow-Origin", "*"))
                .andExpect(header().string("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE"))
                .andExpect(header().string("Access-Control-Allow-Headers", "*"))
                .andExpect(header().string("Access-Control-Max-Age", "3600"));
    }
}
