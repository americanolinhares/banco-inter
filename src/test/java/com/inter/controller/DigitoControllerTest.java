package com.inter.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import com.inter.desafio.controller.DigitoController;
import com.inter.desafio.filter.CorsFilter;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.service.DigitoService;
import com.inter.desafio.service.UsuarioService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class DigitoControllerTest {

    @InjectMocks
    private DigitoController digitoController;

    private MockMvc mockMvc;

    @Mock
    private DigitoService digitoService;

    @Mock
    private UsuarioService usuarioService;

    Digito digito1, digito2;
    Usuario usuario1;
    String digitoJson;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(digitoController).addFilters(new CorsFilter()).build();

        digito1 = new Digito("56", 4);

        digito2 = new Digito();
        digito2.setNumero("10");
        digito2.setRepeticoesNumero(5);
        digito2.setDigitoUnico(5);

        usuario1 = new Usuario(1, "Maria", "maria@bancointer.com");

        digitoJson = "{\"numero\":\"56\",\"repeticoesNumero\":4}";
    }

    @Test
    public void obterTodosDigitosCalculados_deveRetornarComSucesso() throws Exception {

        when(digitoService.obterTodosDigitos()).thenReturn(Arrays.asList(digito1, digito2));

        mockMvc.perform(get("/digitos")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].numero", is("56")))
                .andExpect(jsonPath("$[0].repeticoesNumero", is(4))).andExpect(jsonPath("$[1].numero", is("10")))
                .andExpect(jsonPath("$[1].digitoUnico", is(5)));

        verify(digitoService, times(1)).obterTodosDigitos();
        verifyNoMoreInteractions(digitoService);
    }

    @Test
    public void salvarDigitoSemUsuarioAssociadoSemDigitoCache_deveRetornarCriadoComSucesso() throws Exception {

        when(digitoService.salvarDigito(digito1, "0")).thenReturn(digito1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/digitos").accept(MediaType.APPLICATION_JSON)
                .content(digitoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isCreated()).andExpect(jsonPath("$.numero", is("56")))
                .andExpect(jsonPath("$.repeticoesNumero", is(4)));
    }

    @Test
    public void salvarDigitoSemUsuarioAssociadoComDigitoCache_deveRetornarCriadoComSucesso() throws Exception {

        when(digitoService.salvarDigito(digito1, "0")).thenReturn(digito1);
        when(digitoService.digitoEstaNoCache(digito1)).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/digitos").accept(MediaType.APPLICATION_JSON)
                .content(digitoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isCreated()).andExpect(jsonPath("$.numero", is("56")))
                .andExpect(jsonPath("$.repeticoesNumero", is(4)));
    }

    @Test
    public void salvarDigitoComUsuarioAssociadoComDigitoCache_deveRetornarCriadoComSucesso() throws Exception {

        digito1.setId(1);
        digito1.setDigitoUnico(8);

        when(digitoService.salvarDigito(digito1, "1")).thenReturn(digito1);
        when(usuarioService.obterUsuarioPorId(1)).thenReturn(usuario1);
        when(usuarioService.existeUsuarioPorId(1)).thenReturn(true);
        when(digitoService.digitoEstaNoCache(Mockito.any(Digito.class))).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/digitos?usuario=1")
                .accept(MediaType.APPLICATION_JSON).content(digitoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isCreated()).andExpect(jsonPath("$.numero", is("56")))
                .andExpect(jsonPath("$.repeticoesNumero", is(4)));
    }

    @Test
    public void salvarDigitoComUsuarioAssociadoSemDigitoCache_deveRetornarCriadoComSucesso() throws Exception {

        digito1.setId(1);
        digito1.setDigitoUnico(8);

        when(digitoService.salvarDigito(digito1, "1")).thenReturn(digito1);
        when(usuarioService.obterUsuarioPorId(1)).thenReturn(usuario1);
        when(usuarioService.existeUsuarioPorId(1)).thenReturn(true);
        when(digitoService.digitoEstaNoCache(digito1)).thenReturn(false);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/digitos?usuario=1")
                .accept(MediaType.APPLICATION_JSON).content(digitoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isCreated()).andExpect(jsonPath("$.numero", is("56")))
                .andExpect(jsonPath("$.repeticoesNumero", is(4)));
    }

    @Test
    public void salvarDigitoComUsuarioIncorretoAssociado_deveRetornarNaoModificado() throws Exception {

        digito1.setId(1);
        digito1.setDigitoUnico(8);

        when(digitoService.salvarDigito(digito1, "0")).thenReturn(digito1);
        when(usuarioService.obterUsuarioPorId(3)).thenReturn(null);
        when(digitoService.digitoEstaNoCache(digito1)).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/digitos?usuario=3")
                .accept(MediaType.APPLICATION_JSON).content(digitoJson).contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isNotModified()).andExpect(jsonPath("$.numero", is("56")))
                .andExpect(jsonPath("$.repeticoesNumero", is(4)));
    }

    @Test
    public void cors_headers() throws Exception {
        mockMvc.perform(get("/digitos")).andExpect(header().string("Access-Control-Allow-Origin", "*"))
                .andExpect(header().string("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE"))
                .andExpect(header().string("Access-Control-Allow-Headers", "*"))
                .andExpect(header().string("Access-Control-Max-Age", "3600"));
    }

}
