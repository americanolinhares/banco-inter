package com.inter.desafio.service;

import java.util.ArrayList;
import java.util.List;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public List<Usuario> obterTodosUsuarios() {

        List<Usuario> usuarios = new ArrayList<>();
        usuarioRepository.findAll().forEach(usuario -> usuarios.add(usuario));
        return usuarios;

    }

    public Usuario obterUsuarioPorId(int id) {
        return usuarioRepository.findById(id).get();
    }

    public void deletarUsuarioPorId(int id) {
        usuarioRepository.deleteById(id);
    }

    public Usuario salvarOuAtualizarUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public List<Digito> obterCalculoUsuarioPorId(int id) {
        return usuarioRepository.findById(id).get().getListaDigitosUnicos();
    }

    public boolean existeUsuarioPorId(int id) {
        return usuarioRepository.existsById(id);
    }
}
