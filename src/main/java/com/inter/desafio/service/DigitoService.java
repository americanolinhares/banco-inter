package com.inter.desafio.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.repository.DigitoRepository;
import com.inter.desafio.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DigitoService {

    @Autowired
    DigitoRepository digitoRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    CacheService cacheService;

    public List<Digito> obterTodosDigitos() {
        List<Digito> digitos = new ArrayList<>();
        digitoRepository.findAll().forEach(digito -> digitos.add(digito));
        return digitos;
    }

    public Digito salvarDigito(Digito digito, String idUsuario) {

        // Dígito não associado a Usuário
        if (idUsuario.equals("0")) {
            calcularDigitoUnico(digito);
            digitoRepository.save(digito);
        }

        // Dígito associado a Usuário
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(Integer.parseInt(idUsuario));
        if (optionalUsuario.isPresent()) {
            calcularDigitoUnico(digito);
            Usuario usuario = adicionarDigitoAUsuario(digito, optionalUsuario.get());
            usuarioRepository.save(usuario);
            int numDigitosAssociados = usuario.getListaDigitosUnicos().size();
            digito.setId(usuario.getListaDigitosUnicos().get(numDigitosAssociados - 1).getId());
        }

        return digito;
    }

    public Usuario adicionarDigitoAUsuario(Digito digito, Usuario usuario) {

        ArrayList<Digito> lista = new ArrayList<>(usuario.getListaDigitosUnicos());
        lista.add(digito);
        usuario.setListaDigitosUnicos(lista);
        return usuario;
    }

    public boolean digitoEstaNoCache(Digito digito) {
        return (cacheService.contem(digito.toString()));
    }

    public void inserirCalculoNoCache(Digito digito) {
        cacheService.put(digito.toString(), calcularDigitoUnico(digito).getDigitoUnico());
    }

    public int obterCalculoDoCache(Digito digito) {
        return cacheService.get(digito.toString());
    }

    public Digito calcularDigitoUnico(Digito digito) {

        int digitoUnico = 0;
        BigInteger numeroConcatenado =
                new BigInteger(new String(new char[digito.getRepeticoesNumero()]).replace("\0", digito.getNumero()));
        BigInteger nove = BigInteger.valueOf(9);

        if (!numeroConcatenado.equals(BigInteger.ZERO)) {
            digitoUnico = ((numeroConcatenado.mod(nove).equals(BigInteger.ZERO)) ? nove.intValue()
                    : (numeroConcatenado.mod(nove))).intValue();
        }
        digito.setDigitoUnico(digitoUnico);
        return digito;
    }

}
