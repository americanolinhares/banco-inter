package com.inter.desafio.service;

import org.apache.commons.collections4.map.LRUMap;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    static final int TAMANHO_CACHE = 10;

    private LRUMap<String, Integer> cache;

    public CacheService() {
        cache = new LRUMap<>(TAMANHO_CACHE);
    }

    public void put(String chave, Integer valor) {
        cache.put(chave, valor);
    }

    public Integer get(String chave) {
        if (cache.get(chave) == null) {
            return null;
        } else {
            return cache.get(chave);
        }
    }

    public boolean contem(String chave) {
        return cache.containsKey(chave);
    }

    public int tamanho() {
        return cache.size();
    }
}
