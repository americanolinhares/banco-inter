package com.inter.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacaoBancoInter {

    public static void main(String[] args) {

        SpringApplication.run(AplicacaoBancoInter.class, args);
    }

}
