package com.inter.desafio.controller;

import java.util.List;
import com.inter.desafio.model.Digito;
import com.inter.desafio.model.Usuario;
import com.inter.desafio.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/usuarios")
    private ResponseEntity<List<Usuario>> obterTodos() {

        return new ResponseEntity<>(usuarioService.obterTodosUsuarios(), HttpStatus.OK);
    }

    @GetMapping("/usuarios/{id}")
    private ResponseEntity<Usuario> obterUsuario(@PathVariable("id") int id) {

        if (usuarioService.existeUsuarioPorId(id)) {
            return new ResponseEntity<>(usuarioService.obterUsuarioPorId(id), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/usuarios/{id}")
    private ResponseEntity<Void> deletar(@PathVariable("id") int id) {

        if (usuarioService.existeUsuarioPorId(id)) {
            usuarioService.deletarUsuarioPorId(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/usuarios")
    private ResponseEntity<Usuario> salvarOuAtualizar(@RequestBody Usuario usuarioAtualizado) {

        int idUsuario = usuarioAtualizado.getId();
        Usuario usuario = usuarioService.salvarOuAtualizarUsuario(usuarioAtualizado);

        if (idUsuario != 0) {
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(usuario, HttpStatus.CREATED);
        }
    }

    @GetMapping("/usuarios/calculos/{id}")
    private ResponseEntity<List<Digito>> obterCalculoUsuario(@PathVariable("id") int id) {
        return new ResponseEntity<>(usuarioService.obterCalculoUsuarioPorId(id), HttpStatus.OK);
    }
}
