package com.inter.desafio.controller;

import java.util.List;
import com.inter.desafio.model.Digito;
import com.inter.desafio.service.DigitoService;
import com.inter.desafio.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DigitoController {

    @Autowired
    DigitoService digitoService;

    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/digitos")
    private ResponseEntity<List<Digito>> obterTodos() {

        return new ResponseEntity<>(digitoService.obterTodosDigitos(), HttpStatus.OK);
    }

    @PostMapping("/digitos")
    private ResponseEntity<Digito> salvar(@RequestBody Digito digito,
            @RequestParam(required = false, defaultValue = "0") String usuario) {

        // É enviado '0' quando Digito não está associado à nenhum Usuário
        boolean digitoAssociadoUsuario = !usuario.equals("0");
        boolean existeUsuario = usuarioService.existeUsuarioPorId(Integer.parseInt(usuario));

        // Usuário associado não existe, não é feito o cálculo do Dígito Único
        if (digitoAssociadoUsuario && !existeUsuario) {
            return new ResponseEntity<>(digito, HttpStatus.NOT_MODIFIED);
        }
        // Cálculo ainda não existe no Cache
        if (!digitoService.digitoEstaNoCache(digito)) {
            digitoService.salvarDigito(digito, usuario);
            digitoService.inserirCalculoNoCache(digito);
        } else { // Recupera Cálculo do Cache
            digito.setDigitoUnico(digitoService.obterCalculoDoCache(digito));
        }

        return new ResponseEntity<>(digito, HttpStatus.CREATED);
    }
}
