package com.inter.desafio.model;

import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Usuario {

    @Id
    @GeneratedValue
    private int id;

    private String nome;
    private String email;

    @OneToMany(targetEntity = Digito.class, cascade = CascadeType.ALL)
    private List<Digito> listaDigitosUnicos;

    public Usuario() {
        super();
    }

    public Usuario(int id, String nome, String email) {

        this.setId(id);
        this.setNome(nome);
        this.setEmail(email);
    }

    public Usuario(int id, String nome, String email, List<Digito> listaDigitosUnicos) {

        this.setId(id);
        this.setNome(nome);
        this.setEmail(email);
        this.setListaDigitosUnicos(listaDigitosUnicos);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Digito> getListaDigitosUnicos() {
        if (null == listaDigitosUnicos) {
            return Collections.emptyList();
        }
        return listaDigitosUnicos;
    }

    public void setListaDigitosUnicos(List<Digito> listaDigitosUnicos) {
        this.listaDigitosUnicos = listaDigitosUnicos;
    }
}
