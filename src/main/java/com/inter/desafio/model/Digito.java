package com.inter.desafio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Digito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String numero;
    private int repeticoesNumero;
    private int digitoUnico;

    public Digito() {
        super();
    }

    public Digito(String numero, int repeticoesNumero) {

        this.numero = numero;
        this.repeticoesNumero = repeticoesNumero;

    }

    public Digito(int id, String numero, int repeticoesNumero) {

        this.id = id;
        this.numero = numero;
        this.repeticoesNumero = repeticoesNumero;

    }

    public Digito(int id, String numero, int repeticoesNumero, int digitoUnico) {

        this.id = id;
        this.numero = numero;
        this.repeticoesNumero = repeticoesNumero;
        this.digitoUnico = digitoUnico;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String n) {
        this.numero = n;
    }

    public int getRepeticoesNumero() {
        return repeticoesNumero;
    }

    public void setRepeticoesNumero(int repeticoesNumero) {
        this.repeticoesNumero = repeticoesNumero;
    }

    public int getDigitoUnico() {
        return digitoUnico;
    }

    public void setDigitoUnico(int digitoUnico) {
        this.digitoUnico = digitoUnico;
    }

    @Override
    public String toString() {
        return "Digito [numero=" + getNumero() + "," + " repeticoesNumero=" + getRepeticoesNumero() + "]";
    }
}
