package com.inter.desafio.repository;

import com.inter.desafio.model.Digito;
import org.springframework.data.repository.CrudRepository;

public interface DigitoRepository extends CrudRepository<Digito, Integer> {

}
