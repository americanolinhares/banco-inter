# Aplicação Backend em Java para Banco Inter
Back-end criado para sistema de cálculo de Dígitos Únicos

### Tecnologias/Ferramentas utilizadas:

* JDK 1.8.0_201
* Eclipse Version: 2019-03 (4.11.0)
* Apache Maven 3.5.3
* H2 Database 1.4.199
* Spring Boot Starter Data JPA 2.1.4
* Spring Boot Starter Web 2.1.4
* Spring Boot Starter Test 2.1.4
* JUnit 4.12
* SonarLint 4.1
* EclEmma Java Code Coverage 3.1.2
* Checkstyle 8.18.0
* Postman v7.1.1
* Swagger 2.0
* S.O. Windows

### Execução em ambiente de desenvolvimento usando Eclipse:

* Clonar o projeto:

```sh
$ git clone https://americanolinhares@bitbucket.org/americanolinhares/banco-inter.git
$ cd banco-inter
```

1. Dentro do Eclipse com a perspectiva **Java EE** ir em *File*->*Import*->*Maven*->*Existing Maven Projects*->*Next*;
2. Em *Root Directory* selecionar a pasta com o projeto baixado e depois *Finish*;
3. Clicar com o botão direito na pasta do projeto->*Run as*->*Maven Install*;
4. Dentro do pacote *com.inter.desafio* na pasta **src/main/java** encontrar a classe **AplicacaoBancoInter**;
5. Clicar com o botão direito na classe **AplicacaoBancoInter**->*Run as*->*Java Application*;

### Execução de testes unitários usando Eclipse:

Para executar os testes unitários clicar com o botão direito na pasta do projeto->*Run as*->*Maven test*.

### Execução de testes integrados usando Eclipse e Postman:

1. Para executar os testes integrados deve-se primeiramente executar a aplicação conforme detalhado anteriormente;
2. Deve-se também importar a coleção **postman_collection.json** que está na raiz do repositório clonado pro Postman.
3. Para isto, dentro do Postman clicar em *File*->*Import*->*Import File*->*Choose Files*. Selecionar **postman_collection.json** e clicar em *Abrir*.
4. Com a coleção importada no Postman, passar o cursor sobre a pasta **Coleção Banco Inter** e clicar na setinha.
5. Clicar em *Run*.
6. Uma nova janela se abrirá. Clicar então no botão **Run Coleção Banco Inter** à esquerda na parte inferior da janela.

### Testes (Postman)
![postman-test](https://i.ibb.co/JFb1DVd/calculos-Ana.png)
